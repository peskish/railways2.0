//
//  VenuePointView.swift
//  railways
//
//  Created by Artem Peskishev on 15.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

protocol VenuePointViewDelegate : class {
    func showRouteToVenue(venue : VenuePointView)
    func showRouteFromVenue(venue : VenuePointView)
    func showVenueInfo(venue : NavigationService)
}

class VenuePointView: MapPoint {

    var venue : NavigationService!
    var mapView : UIView!
    var fromButton : UIButton!
    var toButton : UIButton!
    var iconView : UIImageView!
    
    weak var delegate : VenuePointViewDelegate?
    
    init(frame: CGRect, venue: NavigationService) {
        super.init(frame: frame)
        
        self.venue = venue
        
        tag = 1
        
        iconView = UIImageView(frame: frame)
        if let icoURL = venue.ico_url {
            iconView.kf.setImage(with: URL(string:icoURL))
        }
        iconView.contentMode = .scaleAspectFit
        iconView.backgroundColor =  UIColor(red:0.93, green:0.00, blue:0.00, alpha:1.00)
        iconView.layer.cornerRadius = iconView.frame.size.width/2
        iconView.layer.borderColor = UIColor.white.cgColor

        self.addSubview(iconView)
        
        let title = UILabel()
        title.font = UIFont(name:"RobotoCondensed-Regular", size: 16)
        title.textColor = UIColor.white
        title.text = venue.name
        title.sizeToFit()
        
        self.accessibilityLabel = venue.name
        
        var titleWidth = title.frame.size.width + 53
        if titleWidth < 223 {
            titleWidth = 223
        } else if titleWidth > 300 {
            titleWidth = 300
            var frame = title.frame
            frame.size.width = titleWidth - 53
            title.frame = frame
        }
        
        mapView = UIView(frame: CGRect(x: 0, y: 0, width: titleWidth, height: 92))
        mapView?.backgroundColor = UIColor.clear
        mapView?.clipsToBounds = false
        mapView.isUserInteractionEnabled = true
        
        
        let redBg = UIImageView.init(frame: CGRect(x: 0, y: 0, width: titleWidth, height: 44))
        redBg.backgroundColor = UIColor(red:0.93, green:0.00, blue:0.00, alpha:1.00)
        if let color = venue.color_bg {
            redBg.backgroundColor = color.hexColor
            iconView.backgroundColor = color.hexColor
        }
        
        redBg.alpha = 1
        redBg.layer.cornerRadius = redBg.frame.size.height/2
        
        let whiteBG = UIImageView.init(frame: CGRect(x: 0, y: 0, width: titleWidth, height: 92))
        whiteBG.backgroundColor = UIColor.white
        whiteBG.alpha = 1
        whiteBG.layer.cornerRadius = redBg.frame.size.height/2

        mapView.addSubview(whiteBG)
        mapView?.addSubview(redBg)
        
        mapView?.addSubview(title)
        var frame = title.frame
        frame.origin.x = 20
        title.frame = frame
        
        title.center = CGPoint(x: title.center.x, y: redBg.center.y)
        
        let chevroneImageView = UIImageView(frame:CGRect(x: mapView.frame.size.width - 20,y:18,width:10,height:17))
        chevroneImageView.center = CGPoint(x:chevroneImageView.center.x, y: redBg.frame.size.height/2)
        chevroneImageView.image = UIImage(resourceName: "white_arrow")
        mapView.addSubview(chevroneImageView)
        
        let routeImageView = UIImageView(frame: CGRect.zero)
        routeImageView.image = UIImage(resourceName: "route_icon")
        routeImageView.sizeToFit()
        routeImageView.center = CGPoint(x: whiteBG.frame.size.width/2, y: whiteBG.frame.size.height/4+redBg.frame.size.height)
        whiteBG.addSubview(routeImageView)
        
        fromButton = UIButton(type: .system)
        fromButton.setTitle("Отсюда".localized(), for: .normal)
        fromButton.titleLabel?.font = UIFont(name:"RobotoCondensed-Light", size: 16)
        fromButton.tintColor = UIColor.black
        fromButton.sizeToFit()
        fromButton.frame = CGRect(x:0,y:0,width:routeImageView.frame.origin.x,height: 47)
        fromButton.center = CGPoint(x:routeImageView.frame.origin.x/2,y:routeImageView.center.y)
        fromButton.addTarget(self, action: #selector(VenuePointView.fromButtonTapped(button:)), for: .touchUpInside)
        whiteBG.addSubview(fromButton)
        
        toButton = UIButton(type: .system)
        toButton.setTitle("Сюда".localized(), for: .normal)
        toButton.titleLabel?.font = UIFont(name:"RobotoCondensed-Light", size: 16)
        toButton.tintColor = UIColor.black
        toButton.sizeToFit()
        toButton.frame = CGRect(x:0,y:0,width:routeImageView.frame.origin.x,height: 47)
        toButton.center = CGPoint(x:whiteBG.frame.size.width - fromButton.center.x,y:routeImageView.center.y)
        toButton.addTarget(self, action: #selector(VenuePointView.toButtonTapped(button:)), for: .touchUpInside)
        whiteBG.addSubview(toButton)
        
        mapView?.isHidden = true
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let translatedPoint = mapView.convert(point, from: self)
        let fromTP = fromButton.convert(point, from: self)
        let toTP = toButton.convert(point, from: self)
        if (mapView.bounds.contains(translatedPoint)) {
            if mapView.isHidden != true {
                if fromButton.bounds.contains(fromTP) {
                    fromButtonTapped(button: fromButton)
                } else if toButton.bounds.contains(toTP) {
                    toButtonTapped(button: toButton)
                } else {
                    showVenueInfo()
                }
            }

            return mapView.hitTest(translatedPoint, with: event)
        }
        
        return super.hitTest(point, with: event)
    }
    
    @objc func fromButtonTapped(button:UIButton) {
        delegate?.showRouteFromVenue(venue: self)
        mapView.isHidden = true
    }
    
    @objc func toButtonTapped(button:UIButton) {
        delegate?.showRouteToVenue(venue: self)
        mapView.isHidden = true
    }
    
    func showVenueInfo() {
        delegate?.showVenueInfo(venue: self.venue)
        mapView.isHidden = true
    }
    
    func showAnnotation() {
        if mapView.isHidden != false {
            self.addSubview(mapView!)
            mapView?.isHidden = false
            
            
            mapView?.center = CGPoint(x: self.frame.size.width/2, y: -70)
            
            setMapViewFrame()
            
            
            mapView?.alpha = 0
            
            UIView.animate(withDuration: 0.2) {
                self.mapView.center = CGPoint(x: self.mapView.center.x, y: -55)
                self.mapView.alpha = 1
            }
        } else {
//            hideAnnotation()
        }
    }
    
    func setMapViewFrame() {
        if  let superview = superview as? UIScrollView {
            let frame = superview.convert(mapView.frame, from:self)
            let xCor = self.frame.origin.x + self.frame.size.width/2 + mapView.frame.size.width/2
            
            if (frame.origin.x) < CGFloat(0) {
                mapView?.center = CGPoint(x: self.frame.size.width/2 - (frame.origin.x), y: -70)
            } else if xCor > superview.contentSize.width {
                mapView?.center = CGPoint(x: self.frame.size.width/2 + (superview.contentSize.width - xCor), y: -70)
            }
        }
    }
    
    func hideAnnotation() {
        UIView.animate(withDuration: 0.2, animations: {
            self.mapView.center = CGPoint(x: self.mapView.center.x, y: -70)
            self.mapView.alpha = 0
        }, completion: { (complete) in
            self.mapView?.isHidden = true
            self.mapView.removeFromSuperview()
        })
    }
    
    func setBlue() {
        let center = iconView.center
        var frame = iconView.frame
        frame.size = CGSize(width: 33, height: 33)
        iconView?.frame = frame
        iconView.center = center
        iconView.backgroundColor = UIColor(red:0.14, green:0.55, blue:0.89, alpha:1.00)
        iconView.layer.cornerRadius = iconView.frame.size.width/2
        iconView.layer.borderWidth = 2
    }
    
    func setDefault() {
        let center = iconView.center
        var frame = iconView.frame
        frame.size = CGSize(width: 25, height: 25)
        iconView.frame = frame
        iconView.center = center

        iconView.layer.cornerRadius = iconView.frame.size.width/2
        iconView.layer.borderWidth = 0

        if let color = venue.color_bg {
            iconView.backgroundColor = color.hexColor
        } else {
            iconView.backgroundColor =  UIColor(red:0.93, green:0.00, blue:0.00, alpha:1.00)
        }
    }
    
    override func resizeWith(zoom: CGFloat) {

        self.center = CGPoint(x:originalCenter.x*zoom, y: originalCenter.y*zoom)
    }
}

extension String {
    var hexColor: UIColor {
        let hex = trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            return .clear
        }
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
