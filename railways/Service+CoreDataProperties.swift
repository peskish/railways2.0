//
//  Service+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 15.02.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension Service {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Service> {
        return NSFetchRequest<Service>(entityName: "Service");
    }

    @NSManaged public var name: String?
    @NSManaged public var sort: String?
    @NSManaged public var text: String?
    @NSManaged public var photos: NSSet?

}

// MARK: Generated accessors for photos
extension Service {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: ServicePhoto)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: ServicePhoto)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}
