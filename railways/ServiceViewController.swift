//
//  ServiceViewController.swift
//  railways
//
//  Created by Artem Peskishev on 08.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class ServiceViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var stationNameLabel: UILabel!
    
    var station : RailwayStation?
    var textToSet : String?
    var titleString : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let titleString = titleString {
            title = titleString
        }
        
        stationNameLabel.text = station?.name
        
        if let textToSet = textToSet {
            textView.text = textToSet
        }
    }
}
