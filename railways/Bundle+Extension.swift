//
//  Bundle+Extension.swift
//  railways
//
//  Created by Evgeny Karev on 10.05.2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//

import Foundation

extension Bundle {
    public static func stations_frameworkBundle() -> Bundle {
        let bundle = Bundle(for: StationsListTableViewController.self)
        if let path = bundle.path(forResource: "Stations", ofType: "bundle") {
            return Bundle(path: path)!
        } else {
            return bundle
        }
    }
}
