//
//  PhoneAndNavigationCell.swift
//  railways
//
//  Created by Artem Peskishev on 30.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

protocol PhoneAndNavigationCellDelegate : class {
    func navigateToEmergency(emergency : EmergencyPhone)
}

class PhoneAndNavigationCell: UITableViewCell {

    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var navigationButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var phoneButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var buttonsSpace: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var floorLabel: UILabel!
    
    var phoneNumber : String?
    var emergency : EmergencyPhone?
    weak var delegate : PhoneAndNavigationCellDelegate?
    
    func setCell(emergency: EmergencyPhone) {
        titleLabel.text = emergency.name
        self.emergency = emergency
        
        buttonsSpace.constant = 13
        
        if let phone = emergency.phone {
            phoneNumber = phone
            phoneButtonWidth.constant = 35
        } else {
            phoneButtonWidth.constant = 0
        }
        
        if emergency.coordX != 0 {
            navigationButtonWidth.constant = 35
        } else {
            navigationButtonWidth.constant = 0
            buttonsSpace.constant = 0
        }
        
        if let floor = emergency.floor {
            floorLabel.text = "\(floor) " + "этаж".localized()
        } else {
            floorLabel.text = ""
        }
    }
    
    @IBAction func navigationButtonTapped(_ sender: UIButton) {
        delegate?.navigateToEmergency(emergency: self.emergency!)
    }

    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        if let phoneNumber = phoneNumber {
            let formatedNumber = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
            let phoneUrl = "tel://\(formatedNumber)"
            if let url = URL(string: phoneUrl) {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
