//
//  DataBaseManager.swift
//  railways
//
//  Created by Artem Peskishev on 01.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import MagicalRecord

public class DataBaseManager : NSObject {
    
    public  static let shared = DataBaseManager()
    
    
    public  func setupDB() {
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: self.dbStore())
    }
    
    func dbStore() -> String {
        return "\(self.bundleID()).sqlite"
    }
    
    func bundleID() -> String {
        return Bundle.main.bundleIdentifier!
    }
    
    func cleanAndResetupDB() {
        let dbStore = self.dbStore()
        
        let url = NSPersistentStore.mr_url(forStoreName: dbStore)
        let walURL = url?.deletingPathExtension().appendingPathExtension("sqlite-wal")
        let shmURL = url?.deletingPathExtension().appendingPathExtension("sqlite-shm")
        
        MagicalRecord.cleanUp()
        
        //Swift 1
        //let deleteSuccess = NSFileManager.defaultManager().removeItemAtURL(url, error: &removeError)
        
        //Swift 2
        let deleteSuccess: Bool
        do {
            try FileManager.default.removeItem(at: url!)
            try FileManager.default.removeItem(at: walURL!)
            try FileManager.default.removeItem(at: shmURL!)
            deleteSuccess = true
        } catch _ as NSError {
            deleteSuccess = false
        }
        
        if deleteSuccess {
            self.setupDB()
        } else {
            //            println("An error has occured while deleting \(dbStore)")
            //            println("Error description: \(removeError?.description)")
        }
    }
}
