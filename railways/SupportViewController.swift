//
//  SupportViewController.swift
//  railways
//
//  Created by Artem Peskishev on 27.02.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

struct SupportItem {
    var id : String?
    var id_parent : String?
    var name : String?
    var visible : String?
    var sort : Int?
    
    mutating func map(dict: [String: Any]) {
        if let id = dict["id"] as? String {
            self.id = id
        }
        
        if let idParent = dict["id_parent"] as? String {
            self.id_parent = idParent
        }
        
        if let name = dict["name"] as? String {
            self.name = name
        }
        
        if let visible = dict["visible"] as? String {
            self.visible = visible
        }
        
        if let sortStr = dict["sort"] as? String,
            let sort = Int(sortStr) {
            self.sort = sort
        }
    }
}

class SupportViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var venueNameLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    var allitems =  [SupportItem]()
    var items = [SupportItem]()
    var station : RailwayStation?
    var venue : NavigationService?

    var parentItem : SupportItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.reloadData()
        headerTitle.text = "Выберите из списка проблему".localized()
        
        stationLabel.text = station?.name
        
        if let venue = venue {
            DispatchQueue.main.async {
                self.venueNameLabel.text = venue.name
                var frame = self.headerView.frame
                frame.size.height = self.heightForView(text: self.venueNameLabel.text, font: self.venueNameLabel.font, width: self.venueNameLabel.frame.size.width) + 34 + 16
                self.headerView.frame = frame
            }
        } else {
            var frame = headerView.frame
            frame.size.height = 34
            headerView.frame = frame
        }
        
        if parentItem != nil, let name = parentItem?.name {
            
            title = name
            headerTitle.text = "Выберите из списка вариант".localized()
            self.items.sort(by: { Int($0.sort!) < Int($1.sort!) })
            self.items = (self.allitems.filter { $0.id_parent == parentItem?.id })
            self.items = (self.items.filter { $0.visible == "1" })

            self.tableView.reloadData()
            
        } else {
            title = "Исправить неточность".localized()
            tableView.isHidden = true
            
            DataManager.shared.getSupportCategories(completion: { [weak self] (success, error, items) in
                self?.tableView.isHidden = false
                
                if let items = items {
                    for item in items {
                        var supportItem = SupportItem()
                        supportItem.map(dict: item)
                        self?.allitems.append(supportItem)
                    }
                    
                    self?.items = (self?.allitems.filter { $0.id_parent == "0" })!
                    self?.items = (self?.items.filter { $0.visible == "1" })!

                    self?.items.sort(by: { Int($0.sort!) < Int($1.sort!) })

                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    func heightForView(text:String?, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSendSupport" {
            let vc = segue.destination as! SendSupportViewController
            vc.station = station
            vc.venue = venue
            vc.supportItem = sender as! SupportItem?
        }
    }
}

extension SupportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServicesCell
        cell.titleLabel.text = items[indexPath.row].name
        cell.accessoryType = .disclosureIndicator

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedItem = items[indexPath.row]
        let childsArray = (self.allitems.filter { $0.id_parent == selectedItem.id })
        if childsArray.count > 0 {
            let storyboard = UIStoryboard(name: "Navigation", bundle: Bundle.stations_frameworkBundle())
            if let controller = storyboard.instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController {
                controller.parentItem = selectedItem
                controller.station = station
                controller.allitems = allitems
                controller.venue = venue
                self.navigationController?.pushViewController(controller, animated: true)
            }
        } else {
            performSegue(withIdentifier: "showSendSupport", sender: selectedItem)
        }
    }
}

extension SupportViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Пожалуйста, проверьте подключение к интернету".localized()
        let attributes = [NSAttributedStringKey.font : UIFont(name:"RobotoCondensed-Regular", size: 16), NSAttributedStringKey.foregroundColor : UIColor.darkGray]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
