//
//  ServicePhoto+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 15.02.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension ServicePhoto {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ServicePhoto> {
        return NSFetchRequest<ServicePhoto>(entityName: "ServicePhoto");
    }

    @NSManaged public var id_service: String?
    @NSManaged public var url: String?
    @NSManaged public var service: NavigationService?

}
