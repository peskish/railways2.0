//
//  LabelWithInsets.swift
//  railways
//
//  Created by Artem Peskishev on 15.02.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import UIKit

class LabelWithInsets: UILabel {

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }

}
