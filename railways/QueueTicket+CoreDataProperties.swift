//
//  QueueTicket+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 18/03/2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//
//

import Foundation
import CoreData


extension QueueTicket {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<QueueTicket> {
        return NSFetchRequest<QueueTicket>(entityName: "QueueTicket")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var number: String?
    @NSManaged public var ticket_id: String?
    @NSManaged public var wait_count: String?
    @NSManaged public var wait_time: String?
    @NSManaged public var status: String?
    @NSManaged public var status_text: String?
    @NSManaged public var window: String?
    @NSManaged public var station: RailwayStation?

}
