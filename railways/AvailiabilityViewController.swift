//
//  AvailiabilityViewController.swift
//  railways
//
//  Created by Artem Peskishev on 03.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class AvailiabilityViewController: UITableViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var availiabilityLabel: UILabel!
    
    var avail : Availiability?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        availiabilityLabel.text = "Оценка доступности:".localized()
        self.statusView.layer.cornerRadius = self.statusView.frame.size.height/2
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        if let avail = avail {
            if let title = avail.name_cat {
                self.title = title
            }
            
            if let desc = avail.description_cat {
                let attrStr = try! NSMutableAttributedString(
                    data: desc.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ .documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                attrStr.addAttributes([NSAttributedStringKey.font : UIFont(name:"RobotoCondensed-Light", size: 15)!,
                                                                      NSAttributedStringKey.foregroundColor : UIColor.darkGray],
                                      range: NSMakeRange(0, attrStr.string.characters.count))
                descriptionLabel.attributedText = attrStr
            }
            
            if let type1 = Float(avail.type1!),
                let type2 = Float(avail.type2!),
                let type3 = Float(avail.type3!),
                let type4 = Float(avail.type4!) {
                let sum = (type1 + type2 + type3 + type4)/4
                
                percentLabel.text = String(format:"%i%%",Int(sum/2*100))
                statusView.backgroundColor = colorFrom(type: sum)
                
            }
        }
    }
    
    func colorFrom(type:Float) -> UIColor {
        if type == 2 {
            statusLabel.text = "Доступно".localized()
            return UIColor(red:0.27, green:0.61, blue:0.15, alpha:1.00)
        } else if type == 0 {
            statusLabel.text = "Не доступно".localized()
            return UIColor(red:0.93, green:0.00, blue:0.00, alpha:1.00)
        } else {
            statusLabel.text = "Частично доступно".localized()
            return UIColor(red:0.96, green:0.51, blue:0.17, alpha:1.00)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}
