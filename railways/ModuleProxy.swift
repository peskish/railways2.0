//
//  ModuleProxy.swift
//

public protocol ModuleProxyDelegate: class {
    
    func moduleProxy(_ proxy: ModuleProxy, didReceiveRemoteNotificationContent contentString: String)
    
}

open class ModuleProxy {
    
    public weak var delegate: ModuleProxyDelegate?
    
    public static var shared: ModuleProxy = ModuleProxy()
    
    open var deviceId: String {
        return ""
    }
    
    public init() {
        
    }
    
}
