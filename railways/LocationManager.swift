//
//  LocationManager.swift
//  railways
//
//  Created by Artem Peskishev on 23.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationDelegate : class {
    func didUpdateLocation()
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()

    weak var delegate : LocationDelegate?
    
    static let sharedInstance : LocationManager = {
        let locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        let instance = LocationManager(locationManager: locationManager)
        return instance
    }()
    
    init( locationManager : CLLocationManager) {
        self.locationManager = locationManager

    }
    
    func setupLocation() {
        self.locationManager.delegate = self
    }
    
    func currentLocation() -> CLLocation? {
        return self.locationManager.location
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    delegate?.didUpdateLocation()
                }
            }
        }
    }

}
