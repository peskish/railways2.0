//
//  QueueServiceItem.swift
//  railways
//
//  Created by Artem Peskishev on 03/03/2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//

import Foundation

struct QueueServiceItem {
    var parentId: String?
    var operationId: String?
    var title: String?
    var type: String?
}
