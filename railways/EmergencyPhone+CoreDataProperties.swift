//
//  EmergencyPhone+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 16.01.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension EmergencyPhone {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EmergencyPhone> {
        return NSFetchRequest<EmergencyPhone>(entityName: "EmergencyPhone");
    }

    @NSManaged public var coordX: Float
    @NSManaged public var coordY: Float
    @NSManaged public var floor: String?
    @NSManaged public var name: String?
    @NSManaged public var nav_zoneId: String?
    @NSManaged public var phone: String?
    @NSManaged public var station: RailwayStation?

}
