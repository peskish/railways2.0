//
//  PassportCell.swift
//  railways
//
//  Created by Artem Peskishev on 03.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class PassportCell: UITableViewCell {

    @IBOutlet weak var indicatorNameLabel: UILabel!
    @IBOutlet weak var type1Indicator: UIView!
    @IBOutlet weak var type2Indicator: UIView!
    @IBOutlet weak var type3Indicator: UIView!
    @IBOutlet weak var type4Indicator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        type1Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        type2Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        type3Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
        type4Indicator.layer.cornerRadius = type1Indicator.frame.size.height/2
    }
    
    func setCell(avail : Availiability) {
        indicatorNameLabel.text = avail.name_cat
        type1Indicator.backgroundColor = colorFrom(type: avail.type1)
        type2Indicator.backgroundColor = colorFrom(type: avail.type2)
        type3Indicator.backgroundColor = colorFrom(type: avail.type3)
        type4Indicator.backgroundColor = colorFrom(type: avail.type4)

        
    }
    
    func colorFrom(type:String?) -> UIColor {
        if let type = type {
            switch type {
            case "0":
                return UIColor(red:0.93, green:0.00, blue:0.00, alpha:1.00)
            case "1":
                return UIColor(red:0.96, green:0.51, blue:0.17, alpha:1.00)
            case "2":
                return UIColor(red:0.27, green:0.61, blue:0.15, alpha:1.00)
            default:
                return UIColor.white
            }
        } else {return UIColor.white}
    }
}
