//
//  StationInfoViewController.swift
//  railways
//
//  Created by Artem Peskishev on 28.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import Kingfisher
import DZNEmptyDataSet
import MBProgressHUD
import Localize_Swift

class StationInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stationImageView: UIImageView!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var emergencyButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var emergencyButtonLabel: UILabel!
    
    var hud = MBProgressHUD()

    var refreshControl : UIRefreshControl!
    var infoObjects : [Info] = []
    var emergencyObject : Info?
    var station : RailwayStation?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        title = "Меню вокзала".localized()
        emergencyButtonLabel.text = "Экстренные службы".localized()
        if let stationID = station?.stationID {
            self.updateData()
            if infoObjects.count == 0 {
                hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = MBProgressHUDMode.indeterminate
                hud.label.text = "Загрузка".localized()
            }
            DataManager.shared.getInfoForStation(stationID) { (success, error) in
                self.hud.hide(animated: true)
                self.updateData()
            }
        }
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self

        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(StationInfoViewController.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        if let stationID = station?.stationID {
            self.updateData()
            DataManager.shared.getInfoForStation(stationID) { (success, error) in
                self.updateData()
                self.refreshControl.endRefreshing()

            }
        }
    }
    
    func updateData() {
        
        if let stationImage = station?.images {
            stationImageView.kf.setImage(with: URL(string:stationImage))
        }
        
        if let stationName = station?.name {
            stationLabel.text = stationName
        }
        
        self.infoObjects = self.station?.infoObjects?.allObjects as! [Info]
        self.infoObjects.sort(by: { Int($0.sort!)! < Int($1.sort!)! })
        let predicate = NSPredicate(format: "id_parent = nil")
        self.infoObjects = self.infoObjects.filter { predicate.evaluate(with: $0) }
        
        if let emergencyObject = self.infoObjects.filter({$0.name == "emergency_services"}).first {
            self.emergencyObject = emergencyObject
            self.infoObjects.remove(at: self.infoObjects.index(of: emergencyObject)!)
            emergencyButtonHeightConstraint.constant = 50
        } else {
            emergencyButtonHeightConstraint.constant = 0
        }
        
        self.tableView.reloadData()
    }
    
    // MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StationInfoCell") as! StationInfoCell
        cell.nameLabel.text = infoObjects[indexPath.row].title
        if let name = infoObjects[indexPath.row].name {
            cell.setIcon(name: name)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedInfoObject = infoObjects[indexPath.row]
        let predicate = NSPredicate(format: "id_parent = %@", selectedInfoObject.infoID!)
        if let childObjects = Info.mr_findAll(with: predicate), childObjects.count > 0, selectedInfoObject.name != "availability_passport" {
            performSegue(withIdentifier: "showChildInfo", sender: selectedInfoObject)
            return
        }
        
        if let type = selectedInfoObject.type {
            switch type {
            case "html":
                performSegue(withIdentifier: "showHTML", sender: selectedInfoObject)
            case "link":
                performSegue(withIdentifier: "showLink", sender: selectedInfoObject)
            case "json":
                if let name = selectedInfoObject.name {
                    switch name {
                    case "emergency_services":
                        performSegue(withIdentifier: "showEmergency", sender: selectedInfoObject)
                    case "photo":
                        performSegue(withIdentifier: "showPhotos", sender: selectedInfoObject)
                    case "services":
                        if infoObjects.contains(where: { $0.name == "navigation" }) {
                            performSegue(withIdentifier: "showNavigationCategories", sender: selectedInfoObject)
                        } else {
                            performSegue(withIdentifier: "showServices", sender: selectedInfoObject)
                        }

                    case "schedule":
                        performSegue(withIdentifier: "showSchedule", sender: selectedInfoObject)
                    case "availability_passport":
                        performSegue(withIdentifier: "showPassport", sender: selectedInfoObject)
                    case "navigation":
                        performSegue(withIdentifier: "showNavigation", sender: selectedInfoObject)
                    case "queue":
                        performSegue(withIdentifier: "showQMS", sender: selectedInfoObject)
                    default:
                        break
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    @IBAction func emergencyButtonTapped(_ sender: Any) {
        
        if let selectedInfoObject = emergencyObject, let type = selectedInfoObject.type {
            switch type {
            case "html":
                performSegue(withIdentifier: "showHTML", sender: selectedInfoObject)
            case "link":
                performSegue(withIdentifier: "showLink", sender: selectedInfoObject)
            case "json":
                if let name = selectedInfoObject.name {
                    switch name {
                    case "emergency_services":
                        performSegue(withIdentifier: "showEmergency", sender: selectedInfoObject)
                    case "photo":
                        performSegue(withIdentifier: "showPhotos", sender: selectedInfoObject)
                    case "services":
                        performSegue(withIdentifier: "showServices", sender: selectedInfoObject)
                    case "schedule":
                        performSegue(withIdentifier: "showSchedule", sender: selectedInfoObject)
                    case "availability_passport":
                        performSegue(withIdentifier: "showPassport", sender: selectedInfoObject)
                    case "navigation":
                        performSegue(withIdentifier: "showNavigation", sender: selectedInfoObject)
                    default:
                        break
                    }
                }
                break
            default:
                break
            }
        }
    }
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showHTML" {
            let infoObject = sender as! Info
            let vc = segue.destination as! WebViewViewController
            vc.station = station
            vc.htmlString = infoObject.data
            vc.titleString = infoObject.title
            if infoObject.title == "Контакты".localized() {
                vc.showButton = true
            }
        } else if segue.identifier == "showLink" {
            let infoObject = sender as! Info
            let nvc = segue.destination as! UINavigationController
            let vc = nvc.visibleViewController as! LinkViewController
            vc.webURL = infoObject.data
        } else if segue.identifier == "showEmergency" {
            let infoObject = sender as! Info
            let vc = segue.destination as! EmergencyViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showPhotos" {
            let infoObject = sender as! Info
            let vc = segue.destination as! PhotosViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showServices" {
            let infoObject = sender as! Info
            let vc = segue.destination as! ServicesViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showSchedule" {
            let infoObject = sender as! Info
            let vc = segue.destination as! ScheduleViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showPassport" {
            let infoObject = sender as! Info
            let vc = segue.destination as! PassportViewController
            vc.infoID = infoObject.infoID
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showChildInfo" {
            let infoObject = sender as! Info
            let vc = segue.destination as! ChildInfoViewController
            vc.parentObject = infoObject
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showNavigation" {
            let vc = segue.destination as! NavigationViewController
            let infoObject = sender as! Info
            if let jsonString = infoObject.data {
                if let jsonDict = convertToDictionary(text: jsonString),
                    let mapNormal = jsonDict["map_normal"] as? String {
                    vc.locationName = mapNormal
                    if let mapInvalid = jsonDict["map_invalid"] as? String {
                        vc.locationInvName = mapInvalid
                    }
                }
            }
            vc.station = station
        } else if segue.identifier == "showNavigationCategories" {
            let vc = segue.destination as! NavigationCategoriesListViewController
            vc.station = station
        } else if segue.identifier == "showQMS" {
            let infoObject = sender as! Info
            let vc = segue.destination as! QMSViewController
            vc.station = station
            vc.titleString = infoObject.title
            vc.queueObject = infoObject
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension StationInfoViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Пожалуйста, проверьте подключение к интернету".localized()
        let attributes = [NSAttributedStringKey.font : UIFont(name:"RobotoCondensed-Regular", size: 16), NSAttributedStringKey.foregroundColor : UIColor.darkGray]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Потяните, чтобы обновить".localized()
        let attributes = [NSAttributedStringKey.font : UIFont(name:"RobotoCondensed-Regular", size: 14), NSAttributedStringKey.foregroundColor : UIColor.lightGray]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
