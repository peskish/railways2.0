//
//  NavigationViewController.swift
//  railways
//
//  Created by Artem Peskishev on 11.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import MBProgressHUD
import CoreBluetooth
import Reachability
import Navigine

struct RoutePoint {
    var sublocID : Int
    var x : Double
    var y : Double
}

class NavigationViewController: UIViewController {

    let reachability = Reachability()!
    
    @IBOutlet weak var stationNameLabel: UILabel!
    @IBOutlet weak var sv: CustomScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var floorUpButton: UIButton!
    @IBOutlet weak var floorDownButton: UIButton!
    @IBOutlet weak var currentFloorLabel: UILabel!
    @IBOutlet weak var invalidButton: UIButton!
    @IBOutlet weak var invalidButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var invalidView: UIView!
    @IBOutlet weak var invalidViewWidth: NSLayoutConstraint!
    @IBOutlet weak var locateButton: UIButton!
    @IBOutlet weak var feedbackButton: UIButton!
    @IBOutlet weak var directionButton: UIButton!
    
    @IBOutlet weak var routeView: UIView!
    @IBOutlet weak var routeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var routeStartLabel: UILabel!
    @IBOutlet weak var routeFinishLabel: UILabel!
    @IBOutlet weak var wheelChairLabel: UILabel!
    
    var routeLayer = CAShapeLayer()
    var uipath = UIBezierPath()
    var vertexArray = [NCLocationPoint]()
    var startRoutePoint : NCLocationPoint?
    var finishRoutePoint : NCLocationPoint?
    var currentAngle: CGFloat = 0

    var rotationEnabled = false {
        willSet {
            if newValue == false {
                rotateIfNeededWith(azimuth: 0)
            }
        }
        didSet {
            if rotationEnabled == true {
                isFollow = true
                directionButton.isSelected = true
                directionButton.layer.borderWidth = 3
            } else {
                directionButton.isSelected = false
                directionButton.layer.borderWidth = 0
            }
        }
    }
    
    var station : RailwayStation?
    var venuesArray = [NavigationService]()
    var venuesPointsArray = [VenuePointView]()
    var userPosition : UserPositionView!
    var routeVenue : VenuePointView?
    var routeService : NavigationService?
    var routeEmergency : EmergencyPhone?
    @IBOutlet weak var routeButton: UIButton!
    
    var startPin = MapPoint()
    var finishPin = MapPoint()
    var startVenue : VenuePointView? {
        willSet { if startVenue != nil { startVenue?.setDefault() } }
        
        didSet { if startVenue != nil { startVenue?.setBlue()
            sv.bringSubview(toFront: startVenue!)} }
    }
    var finishVenue : VenuePointView? {
        
        willSet { if finishVenue != nil { finishVenue?.setDefault() } }
        
        didSet { if finishVenue != nil { finishVenue?.setBlue()
            sv.bringSubview(toFront: finishVenue!)} }
    }
    
    var timer : Timer!
    var isFollow  = true {
        didSet {
            if isFollow == true {
                locateButton.isSelected = true
                locateButton.layer.borderWidth = 3
                locateButton.alpha = 1
            } else {
                locateButton.isSelected = false
                locateButton.layer.borderWidth = 0
                locateButton.alpha = 0.5
            }
        }
    }
    var locationName : String = ""
    var locationInvName : String = ""
    
    var isRouting = false {
        didSet {
            directionButton.isHidden = !isRouting
            rotationEnabled = isRouting
        }
    }
    
    var currentSublocation : NCSublocation! {
        didSet {
            if let lastChar = currentSublocation.name.last {
                currentFloorLabel.text = String(lastChar)
            } else {
                currentFloorLabel.text = String(format:"%i", navigineManager.location.sublocations.index(of: currentSublocation))
            }
            
            floorDownButton.alpha = 1
            floorUpButton.alpha = 1
            
            let index = navigineManager.location.sublocations.index(of: currentSublocation)
            if index == 0 {
                floorDownButton.alpha = 0.7
            }
            if index == navigineManager.location.sublocations.count-1 {
                floorUpButton.alpha = 0.7
            }
            
            if navigineManager.location.sublocations.count == 1 {
                floorUpButton.isHidden = true
                floorDownButton.isHidden = true
                currentFloorLabel.isHidden = true
            }
            
            showVenuesOnMap()
        }
    }
    
    var sublocationCheckerID : Int = 0
    
    var performSegue = false
    
    var navigineManager = NavigationManager.sharedInstance.navigineManager
    var currentMapVersion : Int = 0
    
    var hud = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Навигация".localized()
        wheelChairLabel.text = "Режим маршрута для маломобильных граждан".localized()
        
        if locationInvName.isEmpty == true {
            invalidButton.isHidden = true
            invalidView.isHidden = true
            invalidButtonHeight.constant = 0
        } else {
            invalidView.layer.cornerRadius = invalidView.frame.size.height/2
            invalidViewWidth.constant = 0
        }
    
        sv.minimumZoomScale = 1
        sv.zoomScale = 1
        sv.maximumZoomScale = 3
        sv.delegate = self
        sv.delaysContentTouches = false
        sv.canCancelContentTouches = true
        sv.clipsToBounds = true
        sv.addSubview(imageView)

        locateButton.layer.borderColor = UIColor.white.cgColor
        locateButton.layer.cornerRadius = locateButton.frame.size.width/2
        locateButton.isSelected = false
        locateButton.layer.borderWidth = 0
        locateButton.alpha = 0.5
        locateButton.isHidden = true
        
        directionButton.layer.borderColor = UIColor.white.cgColor
        directionButton.layer.cornerRadius = locateButton.frame.size.width/2
        directionButton.isSelected = false
        directionButton.layer.borderWidth = 0
        directionButton.isHidden = true
        
        userPosition = UserPositionView(frame:CGRect(x:0, y:0, width: 30, height: 30))
        userPosition.isHidden = true
        sv.addSubview(userPosition)
        
        startPin.isHide = true
        sv.addSubview(startPin)
        
        finishPin.isHide = true
        sv.addSubview(finishPin)
        imageView.isUserInteractionEnabled = true
        routeViewHeight.constant = 0
        sv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        sv.isHidden = true
        
        DispatchQueue.main.async {
            self.loadMap(locname: self.locationName)
        }
        
        if (station?.stationID) != nil {
            stationNameLabel.text = station?.name
        }
        
        let dblTapPress = UITapGestureRecognizer(target: self, action: #selector(NavigationViewController.handleDoubleTap(gesture:)))
        dblTapPress.numberOfTapsRequired = 2
        sv.addGestureRecognizer(dblTapPress)
        
        let tapPress = UITapGestureRecognizer(target: self, action: #selector(NavigationViewController.tapPress(gesture:)))
        tapPress.require(toFail: dblTapPress)
        imageView.addGestureRecognizer(tapPress)
        
        _ = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: true])
        
        navigineManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true

        timer = Timer.scheduledTimer(timeInterval: 0.5,
                                     target: self,
                                     selector: #selector(NavigationViewController.navigationTick),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false

        timer.invalidate()
        performSegue = false
    }
    
    func showError(errorText:String) {
        hud.mode = .text
        hud.label.text = errorText
        hud.hide(animated: true, afterDelay: 3)
    }
    
    func loadMap(locname:String) {
        
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = "Загрузка карты".localized()
        
        navigineManager.downloadLocation(
            byName: locname,
            forceReload: false,
            processBlock: { _ in },
            successBlock: { userInfo in
                DispatchQueue.main.async {
                    if let type = userInfo["type"] as? String, let version = userInfo["version"] as? Int {
                        if type == "local" {
                            self.currentMapVersion = version
                            self.setupNavigine()
                        } else if type == "remote" {
                            if version != self.currentMapVersion {
                                self.setupNavigine()
                            } else {
                                self.navigineManager.startNavigine()
                            }
                        }
                    }
                }
        },
            fail: {error in
                if let error = error as NSError? {
                    DispatchQueue.main.async { [weak self] in
                        if self?.reachability.isReachable != true {
                            self?.showError(errorText: "Требуется подключение к интернету".localized())
                        } else  {
                            self?.showError(errorText: error.localizedDescription)
                        }
                    }
                }
                
        })
    }
    
    @objc func navigationTick() {
        let res = navigineManager.deviceInfo
        if res.error == nil {
            let resSublocId = res.sublocation
            if let resSublocation = navigineManager.location.subLocation(withId: resSublocId),
                let currentSublocation = currentSublocation
            {
                let kx = res.kx
                let ky = res.ky
                
                locateButton.isHidden = false

                if resSublocation.id != sublocationCheckerID {
                    sublocationCheckerID = resSublocation.id
                    changeSublocation(sublocation: resSublocation)
                    
                }
                
                if currentSublocation.id != resSublocation.id && isFollow == true {
                    changeSublocation(sublocation: resSublocation)
                }
                
                rotateIfNeededWith(azimuth: res.azimuth)
                
                if currentSublocation.id != resSublocation.id  {
                    userPosition.isHidden = true
                } else {
                    userPosition.isHidden = false
                }
                
                let mapOriginalWidth = self.imageView.bounds.size.width
                let mapOriginalHeight = self.imageView.bounds.size.height
                
                let xPoint = kx * Float(mapOriginalWidth)
                let yPoint = Float(mapOriginalHeight) - ky * Float(mapOriginalHeight)
                
                userPosition.originalCenter = CGPoint(x: Double(xPoint), y: Double(yPoint))
                userPosition.center = CGPoint(x:userPosition.originalCenter.x*sv.zoomScale, y: userPosition.originalCenter.y*sv.zoomScale)
                
                if isFollow == true {
                    let zoomSize = CGSize(width: sv.bounds.size.width, height: sv.bounds.size.height)
                    var scrollBounds = sv.bounds
                    scrollBounds.origin = CGPoint(x: CGFloat(xPoint)*sv.zoomScale-zoomSize.width/2, y: CGFloat(yPoint)*sv.zoomScale-zoomSize.height/2)
                    
                    sv.bounds = scrollBounds
                }
                
                if isRouting {
                    if let finishVenue = finishVenue {
                        showRouteToVenue(venue: finishVenue)
                    }
                }
            }
        } else {
            userPosition.isHidden = true
            locateButton.isHidden = true
            directionButton.isHidden = true
        }
    }
    
    func setupNavigine() {
        hud.hide(animated: true)
        
        navigineManager.startNavigine()

        if currentSublocation == nil {
            if let firstFloor = navigineManager.location.sublocations.first(where:{($0 as! NCSublocation).name == "floor1"}) as? NCSublocation {
                currentSublocation = firstFloor
            } else {
                currentSublocation = navigineManager.location.sublocations[0] as! NCSublocation
            }
            
            let index = navigineManager.location.sublocations.index(of: currentSublocation)
            if index == 0 {
                floorDownButton.alpha = 0.7
            }
            if index == navigineManager.location.sublocations.count-1 {
                floorUpButton.alpha = 0.7
            }
            
            
            if navigineManager.location.sublocations.count == 1 {
                floorUpButton.isHidden = true
                floorDownButton.isHidden = true
                currentFloorLabel.isHidden = true
            }
        } else {
            currentSublocation = navigineManager.location.subLocation(withId: currentSublocation.id)
        }
        
//        if venuesArray.count == 0 {
            loadVenues()
//        }

        sv.isHidden = false
        
        if let image = UIImage(data: currentSublocation.pngImage) {
            imageView.image = image
            sv.contentSize = imageView.frame.size
            
            var scale : Float = 1
            
            if (image.size.width / image.size.height >
                sv.frame.size.width / sv.frame.size.height){
                scale = Float(sv.frame.size.height) / Float(image.size.height)
            }
            else{
                scale = Float(sv.frame.size.width) / Float(image.size.width)
            }
            imageView.frame = CGRect(x: 0, y: 0, width: image.size.width * CGFloat(scale)*sv.zoomScale, height: image.size.height * CGFloat(scale)*sv.zoomScale)
            imageView.image = image
            sv.contentSize = imageView.frame.size
            
            sv.zoomScale = 2
            let zoomSize = CGSize(width: sv.bounds.size.width, height: sv.bounds.size.height)
            let zoomRect = CGRect(x:imageView.frame.size.width/sv.zoomScale * 0.5,
                                  y:imageView.frame.size.height/sv.zoomScale * 0.5,
                                  width:zoomSize.width,
                                  height:zoomSize.height)
            var scrollBounds = sv.bounds
            scrollBounds.origin = CGPoint(x: zoomRect.origin.x*sv.zoomScale-zoomSize.width/2, y: zoomRect.origin.y*sv.zoomScale-zoomSize.height/2)
            sv.bounds = scrollBounds
            
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 0.5,
                                         target: self,
                                         selector: #selector(NavigationViewController.navigationTick),
                                         userInfo: nil,
                                         repeats: true)

            if let routeService = routeService {
                showRouteToVenue(routeService)
            } else if let emergency = routeEmergency {
                showRouteToPoint(sublocID: Int(emergency.nav_zoneId!)!, coordX: emergency.coordX, coordY: emergency.coordY)
            }                
        }

    }
    
    func changeSublocation(sublocation: NCSublocation) {
        
        if let image = UIImage(data: sublocation.pngImage) {

            imageView.image = image
            sv.contentSize = imageView.frame.size
            var scale : Float = 1
            
            if (image.size.width / image.size.height >
                sv.frame.size.width / sv.frame.size.height){
                scale = Float(sv.frame.size.height) / Float(image.size.height)
            }
            else{
                scale = Float(sv.frame.size.width) / Float(image.size.width)
            }
            imageView.frame = CGRect(x: 0, y: 0, width: image.size.width * CGFloat(scale)*sv.zoomScale, height: image.size.height * CGFloat(scale)*sv.zoomScale)
            imageView.image = image
            sv.contentSize = imageView.frame.size
            
            currentSublocation = sublocation
            
            if routeViewHeight.constant != 0 {
                if startPin.sublocation != currentSublocation {
                    if startPin.isHide != true {
                        startPin.isHidden = true
                    }
                } else {
                    startPin.isHidden = false
                }
                
                if finishPin.sublocation != currentSublocation {
                    if finishPin.isHide != true {
                        finishPin.isHidden = true
                    }
                } else {
                    finishPin.isHidden = false
                }
            }
            
            if vertexArray.count > 0 {
                drawRoute(vertexArray: vertexArray)
            }
        }
    }
    
    func getRotationAngle(azimuth: Float) -> Float {
        let a = Float.pi/4
        if azimuth < a && azimuth > -a {
            return 0
        } else if azimuth > a && azimuth < 3*a {
            return 2*a
        } else if azimuth < -a && azimuth > -3*a {
            return -2*a
        } else {
            return 4*a
        }
    }
    
    func rotateIfNeededWith(azimuth: Float, animated: Bool = true) {
        let rotationAngle = getRotationAngle(azimuth: azimuth)
        
//        if rotationEnabled {
//            UIView.animate(withDuration: animated ? 0.5 : 0, delay: 0, options: .allowUserInteraction, animations: {
//                self.sv.transform = CGAffineTransform(rotationAngle: CGFloat(-rotationAngle))
//                for venue in self.venuesPointsArray {
//                    venue.transform = CGAffineTransform(rotationAngle: CGFloat(rotationAngle))
//                }
//            }, completion: nil)
//        }
        let uzerAzimuth = rotationEnabled ? azimuth : -azimuth
        UIView.animate(withDuration: animated ? 0.5 : 0, delay: 0, options: .allowUserInteraction, animations: {
            self.userPosition.transform = CGAffineTransform(rotationAngle: CGFloat(uzerAzimuth))
        }, completion: nil)
    }
    
    func loadVenues() {
        venuesArray = station?.navigationServices?.allObjects as! [NavigationService]
        showVenuesOnMap()
        if let station = station, let stationID = station.stationID {
            var hud = MBProgressHUD()
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            DataManager.shared.getNavigationServicesForStation(stationID, completion: { (_, _) in
                hud.hide(animated: true)
                self.venuesArray = station.navigationServices?.allObjects as! [NavigationService]
                self.showVenuesOnMap()
            })
        }
    }
    
    func showVenuesOnMap() {
        
        if imageView.image != nil {
            venuesPointsArray.removeAll()
            
            for venue in sv.subviews {
                if venue.tag == 1 {
                    venue.removeFromSuperview()
                }
            }
            
            for venue in venuesArray {
                if let sublocID = venue.nav_zoneId, Int(sublocID) == currentSublocation.id {
                    
                    let mapOriginalWidth = self.imageView.bounds.size.width
                    let mapOriginalHeight = self.imageView.bounds.size.height
                    
                    let xPoint = venue.coordX*Float(mapOriginalWidth)
                    let yPoint = Float(mapOriginalHeight) - venue.coordY * Float(mapOriginalHeight)
                    
                    
                    let venuePoint = VenuePointView(frame:CGRect(x:0, y:0, width: 25, height: 25), venue: venue)
                    venuePoint.originalCenter = CGPoint(x:CGFloat(xPoint),y:CGFloat(yPoint))
                    venuePoint.center = CGPoint(x:venuePoint.originalCenter.x*sv.zoomScale, y: venuePoint.originalCenter.y*sv.zoomScale)
                    
                    venuePoint.addTarget(self, action: #selector(NavigationViewController.showAnnotation(sender:)), for: .touchUpInside)
                    venuePoint.isUserInteractionEnabled = true
                    venuePoint.delegate = self
                    venuePoint.sublocation = currentSublocation
                    
                    if let startVenue = startVenue, startVenue.venue.venID == venuePoint.venue.venID {
                        self.startVenue = venuePoint
                    }
                    
                    if let finishVenue = finishVenue, finishVenue.venue.venID == venuePoint.venue.venID {
                        self.finishVenue = venuePoint
                    }

                    sv.addSubview(venuePoint)
                    venuesPointsArray.append(venuePoint)
                }
            }
            sv.bringSubview(toFront: userPosition)

        }
    }
    
    @objc func showAnnotation(sender : VenuePointView) {
        for venue in venuesPointsArray {
            venue.mapView?.isHidden = true
            venue.mapView.removeFromSuperview()
        }
        sv.bringSubview(toFront: sender)
        sender.showAnnotation()
    }
    
    // TODO: Переделать на NCDevicePath
    func makeRoute(start: NCLocationPoint, finish: NCLocationPoint) {
        let defaultSublocation = navigineManager.location.sublocations[0] as! NCSublocation

        let startY = defaultSublocation.height - Float(start.y) as NSNumber
        let startPoint = NCLocationPoint(location: start.location, sublocation: start.sublocation, x: start.x, y: startY)
        startRoutePoint = start
        
        let finishY = defaultSublocation.height - Float(finish.y) as NSNumber
        let finishPoint = NCLocationPoint(location: finish.location, sublocation: finish.sublocation, x: finish.x, y: finishY)
        finishRoutePoint = finish
        
        let routePointsArray = navigineManager.makeRoute(from: startPoint, to: finishPoint)
        vertexArray = routePointsArray.points as! [NCLocationPoint]
        drawRoute(vertexArray: vertexArray)
    }
    
    func drawRoute(vertexArray:[NCLocationPoint]) {
        routeLayer.removeFromSuperlayer()
        routeLayer = CAShapeLayer()
        uipath.removeAllPoints()
        uipath = UIBezierPath()

        let defaultSublocation = navigineManager.location.sublocations[0] as! NCSublocation

        let ky = Double(imageView.frame.size.height)/Double((defaultSublocation.height))/Double(sv.zoomScale)
        var localPath : UIBezierPath?
        for vertex in vertexArray {
            if vertex.sublocation != currentSublocation.id {
                if let localPath = localPath {
                    uipath.append(localPath)
                }
                localPath = nil
                continue
            }
            
            
            let mapWidthInMeter = CGFloat(defaultSublocation.width)
            let mapOriginalWidth = CGFloat(imageView.bounds.size.width)
            let poX = CGFloat(vertex.x)
            let poY = CGFloat(vertex.y)

            let xPoint = poX * mapOriginalWidth / mapWidthInMeter
//            let yPoint = (poY / CGFloat(navigineManager.location.subLocation(at: 0).height)) * (CGFloat(imageView.frame.size.height) / CGFloat(sv.zoomScale))
            let yPoint = (CGFloat(defaultSublocation.height) - poY)*CGFloat(ky)
            if localPath == nil {
                localPath = UIBezierPath()
                uipath.move(to: CGPoint(x: xPoint, y: yPoint))
            } else {
                uipath.addLine(to: CGPoint(x: xPoint, y: yPoint))
            }
        }
        
        routeLayer.isHidden = false
        routeLayer.path = uipath.cgPath
        routeLayer.strokeColor = UIColor(red:0.09, green:0.55, blue:0.90, alpha:1.0).cgColor
        routeLayer.lineWidth = 3
        routeLayer.fillColor = UIColor.clear.cgColor
        routeLayer.lineDashPattern = [0.2,5]
        routeLayer.lineCap = "round"
        imageView.layer.addSublayer(routeLayer)
    }
    
    
    @objc func tapPress(gesture : UITapGestureRecognizer) {
        if routeViewHeight.constant == 0 {
            removeRouting()
        }
        
        for venue in venuesPointsArray {
            venue.hideAnnotation()
        }
        sv.bringSubview(toFront: userPosition)
    }
    
    
    @IBAction func hideRouteButtonPressed(_ sender: UIButton) {
        removeRouting()
    }
    
    func removeRouting() {
        isRouting = false
        routeButton.isHidden = false
        if startVenue != nil {
            startVenue?.setDefault()
            startVenue = nil
        }
        
        if finishVenue != nil {
            finishVenue?.setDefault()
            finishVenue = nil
        }

        startRoutePoint = nil
        finishRoutePoint = nil
        
        routeViewHeight.constant = 0
        sv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        routeLayer.removeFromSuperlayer()
        vertexArray = []
        routeVenue = nil
    }
    
    @IBAction func floorUpButtonTapped(_ sender: UIButton) {
        if navigineManager.location.sublocations != nil && navigineManager.location.sublocations.count > 0 {
            let sublocation = navigineManager.location.sublocations[navigineManager.location.sublocations.count-1] as! NCSublocation
            if currentSublocation.id == sublocation.id  {
                return
            } else {
                isFollow = false
                let sublocation = navigineManager.location.subLocation(withId: currentSublocation.id)
                currentSublocation = navigineManager.location.sublocations[navigineManager.location.sublocations.index(of: sublocation!) + 1] as! NCSublocation
                
                if currentSublocation == navigineManager.location.sublocations[navigineManager.location.sublocations.count-1] as? NCSublocation {
                    floorUpButton.alpha = 0.7
                }
                changeSublocation(sublocation: currentSublocation)
                floorDownButton.alpha = 1
            }
            
            if vertexArray.count > 0 {
                drawRoute(vertexArray: vertexArray)
            }
        }
    }
    
    @IBAction func floorDownButtonTapped(_ sender: UIButton) {
        if navigineManager.location.sublocations != nil && navigineManager.location.sublocations.count > 0 {
            if navigineManager.location.sublocations.index(of: navigineManager.location.subLocation(withId: currentSublocation.id)) == 0 {
                return
            } else {
                isFollow = false
                
                currentSublocation = navigineManager.location.sublocations[navigineManager.location.sublocations.index(of:currentSublocation) - 1] as! NCSublocation
                
                if navigineManager.location.sublocations.index(of: currentSublocation) == 0 {
                    floorDownButton.alpha = 0.7
                }
                changeSublocation(sublocation: currentSublocation)
                floorUpButton.alpha = 1
            }
            
            if vertexArray.count > 0 {
                drawRoute(vertexArray: vertexArray)
            }
        }
    }
    
    @IBAction func locateButtonTapped(_ sender: UIButton) {
        isFollow = true
    }
    
    @IBAction func invalidButtonTapped(_ sender: UIButton) {
        
        if invalidButton.isSelected != false {
            navigineManager.setGraphTag("default")
            invalidViewWidth.constant = 0
        } else {
            navigineManager.setGraphTag("alternative")
            invalidViewWidth.constant = 172
        }
        
        if let start = startRoutePoint, let finish = finishRoutePoint {
            makeRoute(start: start, finish: finish)
        }
        
        invalidButton.isSelected = !invalidButton.isSelected
    }
    
    @IBAction func directionButtonTapped(_ sender: UIButton) {
        rotationEnabled = !rotationEnabled
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCategories" {
            let vc = segue.destination as! NavigationCategoriesListViewController
            vc.station = station
        } else if segue.identifier == "showVenueInfo" {
            let vc = segue.destination as! NavigationServiceDetailViewController
            vc.station = station
            vc.venue = sender as? NavigationService
            performSegue = true
        } else if segue.identifier == "showFeedback" {
            let vc = segue.destination as! SupportViewController
            vc.station = station
        }
    }
    
    @IBAction func routeButtonTapped(_ sender: UIButton) {
        let res = navigineManager.deviceInfo
        routeButton.isHidden = true
        
        if res.error == nil {
            routeViewHeight.constant = 97
            routeStartLabel.text = "Текущая позиция".localized()
            routeFinishLabel.text = "Укажите конечную точку".localized()
        } else {
            routeViewHeight.constant = 97
            routeStartLabel.text = "Укажите начальную точку".localized()
            routeFinishLabel.text = "Укажите конечную точку".localized()
        }
        
        sv.contentInset = UIEdgeInsets(top: 97, left: 0, bottom: 0, right: 0)
    }
    
    func showRouteToVenue(_ venue: NavigationService) {
        if let sublocation = navigineManager.location.subLocation(withId: Int(venue.nav_zoneId!)!) {
            self.changeSublocation(sublocation: sublocation)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if let venuePoint = self.venuesPointsArray.first(where: {$0.venue.coordX == venue.coordX && $0.venue.coordY == venue.coordY}) {
                    
                    venuePoint.showAnnotation()
                    self.sv.bringSubview(toFront: venuePoint)
                    self.showRouteToVenue(venue: venuePoint)
                    
                    let zoomSize = CGSize(width: self.sv.bounds.size.width, height: self.sv.bounds.size.height)
                    let zoomRect = CGRect(x:self.imageView.frame.size.width/self.sv.zoomScale * CGFloat(venue.coordX),
                                          y:self.imageView.frame.size.height/self.sv.zoomScale*CGFloat((1-venue.coordY)),
                                          width:zoomSize.width,
                                          height:zoomSize.height)
                    self.sv.contentOffset = CGPoint(x: zoomRect.origin.x*self.sv.zoomScale-zoomSize.width/2, y: zoomRect.origin.y*self.sv.zoomScale-zoomSize.height/2)
                }
            }

        }

        
    }
    
    func showRouteToPoint(sublocID: Int, coordX: Float, coordY: Float) {
        
        if let sublocation = navigineManager.location.subLocation(withId: sublocID) {
            self.changeSublocation(sublocation: sublocation)

        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let venuePoint = self.venuesPointsArray.first(where: {$0.venue.coordX == coordX && $0.venue.coordY == coordY}) {
                
                venuePoint.showAnnotation()
                self.sv.bringSubview(toFront: venuePoint)
                self.showRouteToVenue(venue: venuePoint)
                
                let zoomSize = CGSize(width: self.sv.bounds.size.width, height: self.sv.bounds.size.height)
                let zoomRect = CGRect(x:self.imageView.frame.size.width/self.sv.zoomScale * CGFloat(coordX),
                                      y:self.imageView.frame.size.height/self.sv.zoomScale*CGFloat((1-coordY)),
                                      width:zoomSize.width,
                                      height:zoomSize.height)
                self.sv.contentOffset = CGPoint(x: zoomRect.origin.x*self.sv.zoomScale-zoomSize.width/2, y: zoomRect.origin.y*self.sv.zoomScale-zoomSize.height/2)
            }
        }
    }
    
    @objc func handleDoubleTap(gesture : UITapGestureRecognizer) {
        if sv.zoomScale == 1 {
            sv.zoom(to: zoomRectForScale(scale: sv.maximumZoomScale, center: gesture.location(in: gesture.view)), animated: true)
        } else {
            sv.setZoomScale(1, animated: true)
        }
    }
}

extension NavigationViewController : UIScrollViewDelegate {
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        userPosition.resizeWith(zoom: scrollView.zoomScale)
        for venue in venuesPointsArray {
            venue.resizeWith(zoom: scrollView.zoomScale)
        }
        
        startPin.resizeWith(zoom: scrollView.zoomScale)
        finishPin.resizeWith(zoom: scrollView.zoomScale)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        isFollow = false
        rotationEnabled = false
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: sv)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
}

extension NavigationViewController : NavigineCoreDelegate {
    func didRangeVenues(_ venues: [Any]!, _ categories: [Any]!) {
        
    }
    
    func didRangeBeacons(_ beacons: [Any]!) {
        
    }
}

extension NavigationViewController : VenuePointViewDelegate {
    
    func showRouteToVenue(venue : VenuePointView) {
        
        let res = navigineManager.deviceInfo
        let defaultSublocation = navigineManager.location.sublocations[0] as! NCSublocation
        let kx = Double(imageView.frame.size.width)/Double(defaultSublocation.width)/Double(sv.zoomScale)
        let ky = Double(imageView.frame.size.height)/Double(defaultSublocation.height)/Double(sv.zoomScale)

        if startVenue != nil && startVenue == venue {
        } else {
            finishVenue = venue
        }
        
        routeButton.isHidden = true
        routeViewHeight.constant = 97
        sv.contentInset = UIEdgeInsets(top: 97, left: 0, bottom: 0, right: 0)

        routeFinishLabel.text = venue.venue.name

        if res.error == nil {
            if startVenue != nil  {
                if startVenue != venue {
                    routeStartLabel.text = startVenue?.venue.name
                    
                    let startVertex = NCLocationPoint(
                        location: defaultSublocation.location,
                        sublocation: (startVenue?.sublocation?.id)!,
                        x: Double((startVenue?.originalCenter.x)!)/kx as NSNumber,
                        y: Double((startVenue?.originalCenter.y)!)/ky as NSNumber)
                    
                    let finishVertex = NCLocationPoint(
                        location: defaultSublocation.location,
                        sublocation: (venue.sublocation?.id)!,
                        x: Double((venue.originalCenter.x))/kx as NSNumber,
                        y: Double((venue.originalCenter.y))/ky as NSNumber)
                    
                    makeRoute(start: startVertex, finish: finishVertex)
                }
                
            } else {
                if vertexArray.count == 0 {
                    isRouting = true
                }
                
                let startVertex = NCLocationPoint(
                    location: defaultSublocation.location,
                    sublocation: res.sublocation,
                    x: Double((userPosition?.originalCenter.x)!)/kx as NSNumber,
                    y: Double((userPosition?.originalCenter.y)!)/ky as NSNumber)
                
                let finishVertex = NCLocationPoint(
                    location: defaultSublocation.location,
                    sublocation: (venue.sublocation?.id)!,
                    x: Double((venue.originalCenter.x))/kx as NSNumber,
                    y: Double((venue.originalCenter.y))/ky as NSNumber)
                
                makeRoute(start: startVertex, finish: finishVertex)
                
                routeStartLabel.text = "Текущая позиция".localized()
            }
        } else {
            if startVenue != nil {
                if startVenue != venue {
                    routeStartLabel.text = startVenue?.venue.name
                    
                    let startVertex = NCLocationPoint(
                        location: defaultSublocation.location,
                        sublocation: (startVenue?.sublocation?.id)!,
                        x: Double((startVenue?.originalCenter.x)!)/kx as NSNumber,
                        y: Double((startVenue?.originalCenter.y)!)/ky as NSNumber)
                    
                    let finishVertex = NCLocationPoint(
                        location: defaultSublocation.location,
                        sublocation: (venue.sublocation?.id)!,
                        x: Double((venue.originalCenter.x))/kx as NSNumber,
                        y: Double((venue.originalCenter.y))/ky as NSNumber)
                    
                    makeRoute(start: startVertex, finish: finishVertex)
                }
            } else {
                routeStartLabel.text = "Укажите начальную точку".localized()
                routeVenue = venue
            }
        }
    }
    
    func showRouteFromVenue(venue: VenuePointView) {
        if finishVenue != nil && finishVenue == venue {
        } else {
            startVenue = venue
        }
        
        isRouting = false
        
        routeViewHeight.constant = 97
        sv.contentInset = UIEdgeInsets(top: 97, left: 0, bottom: 0, right: 0)

        routeButton.isHidden = true
        routeStartLabel.text = venue.venue.name
        routeFinishLabel.text = "Укажите конечную точку".localized()
        
        let defaultSublocation = navigineManager.location.sublocations[0] as! NCSublocation
        
        let kx = Double(imageView.frame.size.width)/Double(defaultSublocation.width)/Double(sv.zoomScale)
        let ky = Double(imageView.frame.size.height)/Double((defaultSublocation.height))/Double(sv.zoomScale)
        
        if finishVenue != nil {
            if finishVenue != venue {
                routeFinishLabel.text = finishVenue?.venue.name
                
                let startVertex = NCLocationPoint(
                    location: defaultSublocation.location,
                    sublocation: (startVenue?.sublocation?.id)!,
                    x: Double((startVenue?.originalCenter.x)!)/kx as NSNumber,
                    y: Double((startVenue?.originalCenter.y)!)/ky as NSNumber)
                
                let finishVertex = NCLocationPoint(
                    location: defaultSublocation.location,
                    sublocation: (finishVenue?.sublocation?.id)!,
                    x: Double((finishVenue?.originalCenter.x)!)/kx as NSNumber,
                    y: Double((finishVenue?.originalCenter.y)!)/ky as NSNumber)
                
                makeRoute(start: startVertex, finish: finishVertex)
            }
            
        }
    }
    
    func showVenueInfo(venue: NavigationService) {
        if performSegue != true {
            performSegue(withIdentifier: "showVenueInfo", sender: venue)
        }
    }
}

extension NavigationViewController : CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
    }
}
