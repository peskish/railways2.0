//
//  StationInfoCell.swift
//  railways
//
//  Created by Artem Peskishev on 29.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class StationInfoCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func setIcon(name: String) {
        iconImageView.image = nil
        backgroundColor = UIColor.white
        nameLabel.textColor = UIColor(red:0.21, green:0.29, blue:0.35, alpha:1.00)
        switch name {
        case "services":
            iconImageView.image = UIImage(resourceName: "services_icon")
            break
        case "schedule":
            iconImageView.image = UIImage(resourceName: "schedule_icon")
            break
        case "contacts":
            iconImageView.image = UIImage(resourceName: "contacts_icon")
            break
        case "navigation":
            iconImageView.image = UIImage(resourceName: "navigation_icon")
            break
        case "wi-fi":
            iconImageView.image = UIImage(resourceName: "wifi_icon")
            break
        case "availability_passport":
            iconImageView.image = UIImage(resourceName: "passport_icon")
            break
        case "mobility_assistance":
            iconImageView.image = UIImage(resourceName: "mobility_icon")
            break
        case "parking":
            iconImageView.image = UIImage(resourceName: "parking_icon")
            break
        case "history":
            iconImageView.image = UIImage(resourceName: "history_icon")
            break
        case "photo":
            iconImageView.image = UIImage(resourceName: "photos_icon")
            break
        case "panorama":
            iconImageView.image = UIImage(resourceName: "panorama_icon")
            break
        case "emergency_services":
            iconImageView.image = UIImage(resourceName: "emergency_icon")
            backgroundColor = UIColor(red:0.93, green:0.21, blue:0.23, alpha:1.00)
            nameLabel.textColor = UIColor.white
            break
        case "queue":
            iconImageView.image = UIImage(resourceName: "queue")
        default:
            break
        }
    }
}
