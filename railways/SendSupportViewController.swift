//
//  SendSupportViewController.swift
//  railways
//
//  Created by Artem Peskishev on 27.02.17.
//  Copyright © 2017 Enlighted. All rights reserved.
//

import UIKit

class SendSupportViewController: UITableViewController {

    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var addPhotoButton: UIButton!
    
    var supportItem : SupportItem?
    var station : RailwayStation?
    var venue : NavigationService?
    
    var photosArray = [UIImage]()
    let picker = UIImagePickerController()

    var headerView: UIView!
    private let kTableHeaderHeight: CGFloat = 40
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = supportItem?.name
        stationLabel.text = station?.name
        subtitleLabel.text = "Оставьте ваш комментарий".localized()
        addPhotoButton.setTitle("Добавить фотографию".localized(), for: .normal)
        sendButton.setTitle("Отправить".localized(), for: .normal)
        
        picker.delegate = self
        
        sendButton.layer.cornerRadius = sendButton.frame.size.height/2
        
        collectionViewHeight.constant = 0
        tableView.beginUpdates()
        tableView.endUpdates()
        
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 240 //
    }
    
    func updateHeaderView() {
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        headerRect.origin.y = tableView.contentOffset.y
        
        headerView.frame = headerRect
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @IBAction func shootPhoto(_ sender: UIButton) {
        
        if photosArray.count < 3 {
            let alertController = UIAlertController(title: "Выберите источник".localized(), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let galleryAction = UIAlertAction(title: "Галерея".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.picker.allowsEditing = false
                self.picker.sourceType = .photoLibrary
                
                self.present(self.picker, animated: true, completion: nil)
            }
            
            let shootAction = UIAlertAction(title: "Снять фото".localized(), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.picker.allowsEditing = false
                self.picker.sourceType = UIImagePickerControllerSourceType.camera
                self.picker.cameraCaptureMode = .photo
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            }
            
            let cancel = UIAlertAction(title: "Отмена".localized(), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                
            }
            
            alertController.addAction(galleryAction)
            alertController.addAction(shootAction)
            alertController.addAction(cancel)

            
            self.present(alertController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Внимание".localized(), message: "Вы прикрепили максимальное количество фотографий".localized(), preferredStyle: UIAlertControllerStyle.alert)
            
            let cancel = UIAlertAction(title: "Отмена".localized(), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in

            }
            
            alertController.addAction(cancel)

            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func sendFeedback(_ sender: UIButton) {

        
        if commentTextView.text.characters.count > 0 {
            sendButton.isHidden = true
            view.isUserInteractionEnabled = false
            DataManager.shared.sendFeedback(stationID: (station?.stationID)!, cat_id: (supportItem?.id)!, comment: commentTextView.text, photos: photosArray, venue: venue) { (success, error) in
                self.sendButton.isHidden = false
                self.view.isUserInteractionEnabled = true
                
                if success != true {
                    let alertController = UIAlertController(title: "Ошибка".localized(), message: "Что-то пошло не так, попробуйте еще раз".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    
                    let cancel = UIAlertAction(title: "Отмена".localized(), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                        
                    }
                    
                    alertController.addAction(cancel)
                    
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    let alertController = UIAlertController(title: "Спасибо за обратную связь".localized(), message: nil, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let cancel = UIAlertAction(title: "Закрыть".localized(), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                    
                    alertController.addAction(cancel)
                    
                    self.present(alertController, animated: true, completion: nil)
                }

            }
        } else {
            let alertController = UIAlertController(title: nil, message: "Введите комментарий".localized(), preferredStyle: UIAlertControllerStyle.alert)
            
            let cancel = UIAlertAction(title: "Закрыть".localized(), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(cancel)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
}

extension SendSupportViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell",
                                                      for: indexPath) as! PhotoCell
        let photo = photosArray[indexPath.item]
        
        cell.photoImageView.image = photo
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.frame.size.width
        
        return CGSize(width: (collectionViewWidth - 36)/3 , height: (collectionViewWidth - 36)/3 )
    }
}


extension SendSupportViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        photosArray.append(chosenImage)
        collectionView.reloadData()
        collectionViewHeight.constant = (collectionView.frame.size.width - 36)/3
        
        tableView.beginUpdates()
        tableView.endUpdates()
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
