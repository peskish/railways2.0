//
//  PhotosViewController.swift
//  railways
//
//  Created by Artem Peskishev on 30.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class PhotosViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var station : RailwayStation?
    var titleString : String?
    
    var photosArray : [Photo] = []
    var images = [SKPhoto]()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let titleString = titleString {
            title = titleString
        }
        
        if let station = station {
            titleLabel.text = station.name
            if let photos = station.photos {
                photosArray = photos.allObjects as! [Photo]
                photosArray = photosArray.sorted(by: { $0.photoID! < $1.photoID! })
                prepareSKPhotos()
                collectionView.reloadData()
            }
        }
        
        if let stationID = station?.stationID {
            DataManager.shared.getPhotosForStation(stationID, completion: {_,_ in
                if let station = self.station {
                    if let photos = station.photos {
                        self.photosArray = photos.allObjects as! [Photo]
                        self.photosArray = self.photosArray.sorted(by: { $0.photoID! < $1.photoID! })
                        self.prepareSKPhotos()
                        self.collectionView.reloadData()
                    }
                }
            })
        }
    }

    func prepareSKPhotos() {
        images = [SKPhoto]()
        for photo in photosArray {
            if let url = photo.url {
                let skPhoto = SKPhoto.photoWithImageURL(url)
                images.append(skPhoto)
                skPhoto.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell",
                                                      for: indexPath) as! PhotoCell
        if let url = photosArray[indexPath.item].url {
            cell.photoImageView.kf.setImage(with: URL(string:url))
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let browser = PhotoBrowser(photos: images)
        browser.initializePageIndex(indexPath.item)
        present(browser, animated: true, completion: {})

    }
}
