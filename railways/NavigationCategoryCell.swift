//
//  NavigationServiceCell.swift
//  railways
//
//  Created by Artem Peskishev on 20.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class NavigationCategoryCell: UITableViewCell {

    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var floorLabel: UILabel!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCell(_ category:NavigationCategory) {

        floorLabel.text = ""

        if let imgUrl = category.ico_url {
            serviceImageView.kf.setImage(with: URL(string:imgUrl))
        }
        
        if let name = category.name {
            serviceLabel.text = name
        }
    }
    
    func setCell(venue:NavigationService) {
        
        if let name = venue.name {
            serviceLabel.text = name
        }
        
        if let catID = venue.id_cat, let category = NavigationCategory.mr_findFirst(byAttribute: "categoryID", withValue: catID) {
            if let imgUrl = category.ico_url {
                serviceImageView.kf.setImage(with: URL(string:imgUrl))
            }
        }
        
        if let floor = venue.floor {
            floorLabel.text = "\(floor) " + "этаж".localized()
        } else {
            floorLabel.text = ""
        }
    }

}
