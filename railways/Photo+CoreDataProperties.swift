//
//  Photo+CoreDataProperties.swift
//  railways
//
//  Created by Artem Peskishev on 30.11.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo");
    }

    @NSManaged public var photoID: String?
    @NSManaged public var url: String?
    @NSManaged public var railway: RailwayStation?

}
