//
//  NavigationServicesListViewController.swift
//  railways
//
//  Created by Artem Peskishev on 20.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import MBProgressHUD

class NavigationCategoriesListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    var station : RailwayStation?
    var categoriesArray = [NavigationCategory]()
    var servicesArray = [NavigationService]()
    var hud = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        title = "Навигация".localized()
        searchField.placeholder = "поиск места/услуги".localized()
        
        if let station = station {
            stationLabel.text = station.name
            
            categoriesArray = station.navigationCategories?.allObjects as! [NavigationCategory]
            sortCategoriesArray()
            tableView.reloadData()
            
            if let stationID = station.stationID {
                if categoriesArray.count == 0 {
                    hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                    hud.mode = MBProgressHUDMode.indeterminate
                }
                DataManager.shared.getNavigationServicesForStation(stationID, completion: { (_, _) in
                    self.hud.hide(animated: true)
                    if let searchString = self.searchField.text, searchString.count > 0 {
                        
                    } else {
                        self.categoriesArray = station.navigationCategories?.allObjects as! [NavigationCategory]
                        self.sortCategoriesArray()
                        self.tableView.reloadData()
                    }
                })
            }
        }
    }
    
    func sortCategoriesArray() {
        categoriesArray.sort{
            if $0.sort != $1.sort {
                return $0.sort! < $1.sort!
            }
            else {
                return $0.name! < $1.name!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArray.count + servicesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NavigationCategoryCell") as! NavigationCategoryCell

        if indexPath.row < categoriesArray.count {
            
            let category = categoriesArray[indexPath.row]
            cell.setCell(category)
            
            if let searchString = searchField.text, searchString.count > 0, let catName = category.name {
                let attributedString = NSMutableAttributedString(string: catName)
                
                do {
                    let regex = try NSRegularExpression(pattern: searchString, options: .caseInsensitive)
                    let range = NSRange(location: 0, length: catName.utf16.count)
                    for match in regex.matches(in: catName, options: .withTransparentBounds, range: range) {
                        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: match.range)
                    }
                    cell.serviceLabel.attributedText = attributedString
                } catch _ {
                    NSLog("Error creating regular expresion")
                }
            }


        } else {
            
            let sevice = servicesArray[indexPath.row-categoriesArray.count]
            cell.setCell(venue:sevice)
            
            if let searchString = searchField.text, searchString.count > 0, let serName = sevice.name {
                let attributedString = NSMutableAttributedString(string: serName)
                
                do {
                    let regex = try NSRegularExpression(pattern: searchString, options: .caseInsensitive)
                    let range = NSRange(location: 0, length: serName.utf16.count)
                    for match in regex.matches(in: serName, options: .withTransparentBounds, range: range) {
                        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: match.range)
                    }
                    cell.serviceLabel.attributedText = attributedString
                } catch _ {
                    NSLog("Error creating regular expresion")
                }
            }


        }
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        view.endEditing(true)
        if indexPath.row < categoriesArray.count {
            let category = categoriesArray[indexPath.row]
            performSegue(withIdentifier: "showServices", sender: category)
        } else {
            let service = servicesArray[indexPath.row - categoriesArray.count]
            performSegue(withIdentifier: "showService", sender: service)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showServices" {
            let vc = segue.destination as! NavigationServicesViewController
            vc.category = sender as? NavigationCategory
            vc.station = station
        } else if segue.identifier == "showService" {
            let vc = segue.destination as! NavigationServiceDetailViewController
            vc.station = station
            vc.venue = sender as? NavigationService
            
        } else if segue.identifier == "showMap" {
            let vc = segue.destination as! NavigationViewController
            vc.station = station
            
            let predicate = NSPredicate(format: "name == %@ AND railwayStation.stationID == %@", "navigation", (station?.stationID)!)
            if let infoObject = Info.mr_findFirst(with: predicate) {
                if let jsonString = infoObject.data {
                    if let jsonDict = convertToDictionary(text: jsonString),
                        let mapNormal = jsonDict["map_normal"] as? String {
                        vc.locationName = mapNormal
                        if let mapInvalid = jsonDict["map_invalid"] as? String {
                            vc.locationInvName = mapInvalid
                        }
                    }
                }
            }

        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    @IBAction func mapButtonTapped(_ sender: UIBarButtonItem) {
        for vc in (navigationController?.viewControllers)! {
            if vc is NavigationViewController {
                _ = self.navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
        
        performSegue(withIdentifier: "showMap", sender: nil)
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        if searchButton.isSelected == true {
            searchButton.isSelected = false
            searchField.text = ""
            view.endEditing(true)
            self.categoriesArray = station?.navigationCategories?.allObjects as! [NavigationCategory]
            sortCategoriesArray()
            servicesArray.removeAll()
            self.tableView.reloadData()
        }
    }
    
    @IBAction func searchFieldEditingChanged(_ sender: UITextField) {
        if let searchString = sender.text, searchString.count > 0 {
            let predicate = NSPredicate(format: "name contains[c] %@", searchString)

            categoriesArray.removeAll()
            servicesArray = NavigationService.mr_findAll(with: predicate) as! [NavigationService]
            
            tableView.reloadData()
            searchButton.isSelected = true
        } else {
            searchButton.isSelected = false
            self.categoriesArray = station?.navigationCategories?.allObjects as! [NavigationCategory]
            sortCategoriesArray()
            servicesArray.removeAll()
            self.tableView.reloadData()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}
