import UIKit

class QueueTicketView: UIView {
    @IBOutlet weak var checkViewButton: UIButton!
    @IBOutlet weak var waitCountLabel: UILabel!
    @IBOutlet weak var ticketNumberLabel: UILabel!
    @IBOutlet weak var ticketDateLabel: UILabel!
    @IBOutlet weak var ticketWaitTimeLabel: UILabel!
    @IBOutlet weak var waitContainer: UIView!
    @IBOutlet weak var waitTimeContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var waitCountContainer: UIView!
    @IBOutlet weak var waitCountContainerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var statusContainerView: UIView!
    @IBOutlet weak var statusTextLabel: UILabel!
    @IBOutlet weak var supportMessageContainer: UIView!
    @IBOutlet weak var windowLabel: UILabel!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    @IBOutlet weak var ticketNumberDescLabel: UILabel!
    @IBOutlet weak var ticketDateDescLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var averageWaitTimeDescLabel: UILabel!
    @IBOutlet weak var peopleInFrontLabel: UILabel!
    func setTicket(_ ticket: QueueTicket) {
        
        ticketNumberDescLabel.text = "Ваш номер в очереди:".localized()
        ticketDateDescLabel.text = "Дата регистрации:".localized()
        minLabel.text = "мин".localized()
        averageWaitTimeDescLabel.text = "Примерное время  ожидания:".localized()
        peopleInFrontLabel.text = "Человек в очереди перед вами:".localized()
        errorMessageLabel.text = "Попробуйте встать в очередь через 5 минут".localized()
        checkViewButton.setTitle("Выйти из очереди".localized(), for: .normal)
        
        checkViewButton.layer.cornerRadius = checkViewButton.frame.size.height/2
        checkViewButton.setTitle("Отменить".localized(), for: .normal)
        statusContainerView.isHidden = true
        supportMessageContainer.isHidden = true
        windowLabel.isHidden = true
        errorMessageLabel.isHidden = true
        waitCountContainer.isHidden = false
        waitCountContainerHeight.constant = 60
        waitContainer.isHidden = false
        waitTimeContainerHeight.constant = 60
        
        
        waitCountLabel.text = ticket.wait_count
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.long
        dateformatter.dateFormat = "HH:mm, dd MMMM YYYY "
        dateformatter.timeZone = NSTimeZone(name:"GMT") as TimeZone!
        if let date = ticket.date {
            let formattedString = dateformatter.string(from: date as! Date)
            ticketDateLabel.text = formattedString
        }
        
        ticketNumberLabel.text = ticket.number
        ticketWaitTimeLabel.text = ticket.wait_time
        
        if ticket.wait_time == nil || ticket.wait_time == "" {
            waitContainer.isHidden = true
            waitTimeContainerHeight.constant = 0
        }
        
        if ticket.wait_count == nil || ticket.wait_count == "" {
            waitCountContainer.isHidden = true
            waitCountContainerHeight.constant = 0
        }

        if let status = ticket.status {
            switch status {
            case "0":
                // ошибка
                waitContainer.isHidden = false
                waitCountContainer.isHidden = false
                statusContainerView.isHidden = false
                
                statusContainerView.backgroundColor = UIColor(red:0.85, green:0.18, blue:0.13, alpha:1.00)
                statusTextLabel.text = ticket.status_text
                let labelHeight = estimatedHeightOfLabel(text: ticket.status_text)
                waitTimeContainerHeight.constant = labelHeight > 44 ? labelHeight + 16 : 60
                checkViewButton.setTitle("Закрыть".localized(), for: .normal)
                errorMessageLabel.isHidden = false
                waitCountContainerHeight.constant = 60
                supportMessageContainer.isHidden = false
            case "1":
                statusContainerView.isHidden = true
                // ожидание
                break
            case "5":
                // вызов
                waitContainer.isHidden = false
                waitCountContainer.isHidden = false
                statusContainerView.isHidden = false
                statusContainerView.backgroundColor = UIColor(red:0.50, green:0.74, blue:0.38, alpha:1.00)
                statusTextLabel.text = ticket.status_text
                let labelHeight = estimatedHeightOfLabel(text: ticket.status_text)
                waitTimeContainerHeight.constant = labelHeight > 44 ? labelHeight + 16 : 60
                windowLabel.isHidden = false
                if let window = ticket.window {
                    windowLabel.text = "№\(window)"
                    supportMessageContainer.isHidden = false
                    waitCountContainerHeight.constant = 60
                } else {
                    waitCountContainer.isHidden = true
                    waitCountContainerHeight.constant = 0
                }

            case "9":
                // Обслуживание завершено
                waitContainer.isHidden = false
                statusContainerView.isHidden = false
                statusContainerView.backgroundColor = UIColor(red:0.50, green:0.74, blue:0.38, alpha:1.00)
                statusTextLabel.text = ticket.status_text
                let labelHeight = estimatedHeightOfLabel(text: ticket.status_text)
                waitTimeContainerHeight.constant =  labelHeight > 44 ? labelHeight + 16 : 60
                waitCountContainer.isHidden = true
                waitCountContainerHeight.constant = 0
                checkViewButton.setTitle("Закрыть".localized(), for: .normal)
            default:
                break
            }
        }
    }
    
    func estimatedHeightOfLabel(text: String?) -> CGFloat {
        guard let text = text else { return 0 }
        let size = CGSize(width: frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedStringKey.font: UIFont(name:"RobotoCondensed-Regular", size: 15)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
}
