import UIKit

class DetailQMSViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var servicesList = [QueueServiceItem]()
    var filteredList = [QueueServiceItem]()
    var station : RailwayStation?
    var titleString : String?
    var onComplete: ((String) -> ())?
    
    
    @IBOutlet weak var stationNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let station = station {
            stationNameLabel.text = station.name
        }
        
        title = "Выберите услугу".localized()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServicesCell
        cell.titleLabel.text = filteredList[indexPath.row].title
        
        if let operationId = filteredList[indexPath.row].operationId {
            let childArray = servicesList.filter({ $0.parentId == operationId})
            if childArray.count == 0 {
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .disclosureIndicator
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let operationId = filteredList[indexPath.row].operationId {
            let childArray = servicesList.filter({ $0.parentId == operationId})
            if childArray.count > 0 {
                performSegue(withIdentifier: "showDetailQMS", sender: childArray)
            } else {
                for vc in (navigationController?.viewControllers)! {
                    if vc is QMSViewController {
                        onComplete?(operationId)
                        _ = self.navigationController?.popToViewController(vc, animated: true)
                        return
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailQMS" {
            let vc = segue.destination as! DetailQMSViewController
            vc.servicesList = servicesList
            vc.filteredList = sender as! [QueueServiceItem]
            vc.station = station
            vc.onComplete = onComplete
        }
    }
}
