//
//  String+Extension.swift
//  railways
//
//  Created by Evgeny Karev on 10.05.2018.
//  Copyright © 2018 Enlighted. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return localized(using: "Localizable", in: Bundle.stations_frameworkBundle())
    }
    
    func localizedFormat(_ arguments: CVarArg...) -> String {
        return String(format: localized(using: "Localizable", in: Bundle.stations_frameworkBundle()), arguments: arguments)
    }
}
