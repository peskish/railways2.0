//
//  NavigationServiceDetailViewController.swift
//  railways
//
//  Created by Artem Peskishev on 21.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import TTTAttributedLabel

class NavigationServiceDetailViewController: UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var phoneLabel: TTTAttributedLabel!
    @IBOutlet weak var phone2Label: TTTAttributedLabel!
    @IBOutlet weak var mailLabel: TTTAttributedLabel!
    @IBOutlet weak var siteLabel: TTTAttributedLabel!
    @IBOutlet weak var phoneHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var weekdaysWorkHoursLabel: UILabel!
    @IBOutlet weak var weeldaysWorkHoursDescLabel: UILabel!
    @IBOutlet weak var weekendWorkHoursLabel: LabelWithInsets!
    @IBOutlet weak var weekendWorkHoursDescLabel: UILabel!
    @IBOutlet weak var workHoursLabel: UILabel!

    @IBOutlet weak var floorLabel: UILabel!
    @IBOutlet weak var floorView: UIView!

    @IBOutlet weak var callLabel: UILabel!
    @IBOutlet weak var phoneIcon: UIButton!
    @IBOutlet weak var routeLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var stationView: UIView!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var photoCountLabel: UILabel!
    @IBOutlet weak var photoCountView: UIView!
    
    @IBOutlet weak var siteDescLabel: UILabel!
    @IBOutlet weak var emailDescLabel: UILabel!
    @IBOutlet weak var phone2DescLabel: UILabel!
    @IBOutlet weak var phoneDescLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var fixErrorLabel: UILabel!
    @IBOutlet weak var fixIssueView: UIView!
    var footerView: UIView!
    
    var venue : NavigationService?
    var station : RailwayStation?
    
    var photosArray : [ServicePhoto] = []
    var images = [SKPhoto]()
    
    var headerView: UIView! 
    private let kTableHeaderHeight: CGFloat = 40

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let venue = venue {
            title = venue.name
            
            if let imageURL = venue.img_url {
                imageView.kf.setImage(with: URL(string:imageURL))
            }
            callLabel.text = "Позвонить".localized()
            routeLabel.text = "Проложить маршрут".localized()
            phoneDescLabel.text = "Телефон".localized()
            fixErrorLabel.text = "Исправить неточность".localized()
            siteDescLabel.text = "Сайт".localized()
            workHoursLabel.text = "Режим работы".localized()
            weeldaysWorkHoursDescLabel.text = "Будни".localized()
            weekdaysWorkHoursLabel.text = "Выходные".localized()
            stationLabel.text = station?.name
            
            if let desc = venue.text {
                let attrStr = try! NSMutableAttributedString(
                    data: desc.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ .documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                attrStr.addAttributes([NSAttributedStringKey.font : UIFont(name:"RobotoCondensed-Light", size: 15)!,
                                       NSAttributedStringKey.foregroundColor : UIColor.darkGray],
                                      range: NSMakeRange(0, attrStr.string.count))
                descriptionLabel.attributedText = attrStr
            }
            
            var linkAttributes = [AnyHashable : Any]()
            linkAttributes[NSAttributedStringKey.foregroundColor] = UIColor(red:0.21, green:0.29, blue:0.35, alpha:1.0)
            
            phoneLabel.enabledTextCheckingTypes = NSTextCheckingAllTypes
            phoneLabel.delegate = self
            phoneLabel.linkAttributes = linkAttributes
            phone2Label.enabledTextCheckingTypes = NSTextCheckingAllTypes
            phone2Label.delegate = self
            phone2Label.linkAttributes = linkAttributes
            mailLabel.enabledTextCheckingTypes = NSTextCheckingAllTypes
            mailLabel.delegate = self
            mailLabel.linkAttributes = linkAttributes
            siteLabel.enabledTextCheckingTypes = NSTextCheckingAllTypes
            siteLabel.delegate = self
            siteLabel.linkAttributes = linkAttributes
            
            nameLabel.text = venue.name
            phoneLabel.text = venue.tel
            phone2Label.text = venue.tel2
            mailLabel.text = venue.email
            siteLabel.text = venue.site
            if let floor = venue.floor {
                floorLabel.text = floor + " " + "этаж".localized()
            } else {
                floorView.isHidden = true
            }
            
            
            
            if venue.tel == nil && venue.tel2 == nil {
                phoneHeightConstraint.constant = 1
                phoneIcon.isHidden = true
                callLabel.isHidden = true
            }
            
            weekendWorkHoursLabel.text = venue.mode_work_weekend
            weekdaysWorkHoursLabel.text = venue.mode_work_weekdays
            
            if weekdaysWorkHoursLabel.text == nil && weekdaysWorkHoursLabel.text==nil {
                workHoursLabel.text = ""
            }
            
            if weekdaysWorkHoursLabel.text == nil {
                weeldaysWorkHoursDescLabel.text = ""
            }
            
            if weekendWorkHoursLabel.text == nil {
                weekendWorkHoursDescLabel.text = ""
            }
            
            headerView = tableView.tableHeaderView
            tableView.tableHeaderView = nil
            tableView.addSubview(headerView)
            
            footerView = tableView.tableFooterView
            tableView.tableFooterView = nil
            tableView.addSubview(footerView)
            
            footerView.frame = CGRect(x:0, y: view.frame.size.height - footerView.frame.size.height, width: view.frame.size.width, height: footerView.frame.size.height)
            
            tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: footerView.frame.size.height, right: 0)
            tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
            
            
            if let photos = venue.photos, photos.count > 0 {
                photosArray = venue.photos?.allObjects as! [ServicePhoto]
                photosArray = photosArray.sorted(by: { $0.url! < $1.url! })
                photoCountLabel.text = "1"+"/"+"\(photosArray.count)"
                
                prepareSKPhotos()
                if photos.count < 2 {
                    photoCountView.isHidden = true
                }
            } else {
                photoCountView.isHidden = true
            }
            
            if phoneLabel.text == nil {
                phoneDescLabel.text = ""
            }
            
            if phone2Label.text == nil {
                phone2DescLabel.text = ""
            }
            
            if mailLabel.text == nil {
                emailDescLabel.text = ""
            }
            
            if siteLabel.text == nil {
                siteDescLabel.text = ""
            }
            
            DispatchQueue.main.async {
                self.updateHeaderView()
            }
            
        }
    }
    
    func prepareSKPhotos() {
        images = [SKPhoto]()
        for photo in photosArray {
            if let url = photo.url {
                let skPhoto = SKPhoto.photoWithImageURL(url)
                images.append(skPhoto)
                skPhoto.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            }
        }
    }
    
    func updateHeaderView() {
        var headerRect = CGRect(x: 0, y: -kTableHeaderHeight, width: tableView.bounds.width, height: kTableHeaderHeight)
        headerRect.origin.y = tableView.contentOffset.y

        headerView.frame = headerRect
        
        var footerRect = CGRect(x:0, y: view.frame.size.height-kTableHeaderHeight - footerView.frame.size.height, width: view.frame.size.width, height: footerView.frame.size.height)
        footerRect.origin.y = tableView.contentOffset.y + (view.frame.size.height - footerView.frame.size.height)
        footerView.frame = footerRect
    }
    
    @IBAction func showRoute(_ sender: UIButton) {
        for vc in (navigationController?.viewControllers)! {
            if let nvc = vc as? NavigationViewController {
                _ = navigationController?.popToViewController(nvc, animated: true)
                nvc.showRouteToVenue(venue!)
                return
            }
        }
        
        performSegue(withIdentifier: "showMap", sender: nil)
    }
    
    @IBAction func callPhone(_ sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let tel = venue?.tel {
            let telAction = UIAlertAction(title: tel, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let trimmedString = tel.trimmingCharacters(in: NSCharacterSet.whitespaces)
                let telUrl:NSURL? = NSURL(string:"telprompt:"+trimmedString)
                if ((telUrl) != nil){
                    if(UIApplication.shared.canOpenURL(telUrl! as URL)){
                        UIApplication.shared.openURL(NSURL(string: "telprompt:"+trimmedString)! as URL)
                    }else
                    {
                        print("Call not available")
                    }
                }

            })
            
            optionMenu.addAction(telAction)
        }
        
        if let tel2 = venue?.tel2 {
            let telAction = UIAlertAction(title: tel2, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let trimmedString = tel2.trimmingCharacters(in: NSCharacterSet.whitespaces)
                let telUrl:NSURL? = NSURL(string:"telprompt:"+trimmedString)
                if ((telUrl) != nil){
                    if(UIApplication.shared.canOpenURL(telUrl! as URL)){
                        UIApplication.shared.openURL(NSURL(string: "telprompt:"+trimmedString)! as URL)
                    }else
                    {
                        print("Call not available")
                    }
                }
            })
            
            optionMenu.addAction(telAction)
        }
        

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
    
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 2:
            if venue?.tel == nil{
                return 0
            }
        case 3:
            if venue?.tel2 == nil {
                return 0
            }
        case 4:
            if venue?.email == nil {
                return 0
            }
        case 5:
            if venue?.site == nil {
                return 0
            }
        case 6:
            if venue?.mode_work_weekdays == nil && venue?.mode_work_weekend == nil {
                return 0
            }
        case 7:
            if venue?.mode_work_weekdays == nil {
                return 0
            }
            
        case 8:
            if venue?.mode_work_weekend == nil {
                return 0
            }
        default:
            return UITableViewAutomaticDimension
        }
        return UITableViewAutomaticDimension

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let vc = segue.destination as! NavigationViewController
            vc.station = station
            vc.routeService = venue
            
            let predicate = NSPredicate(format: "name == %@ AND railwayStation.stationID == %@", "navigation", (station?.stationID)!)
            if let infoObject = Info.mr_findFirst(with: predicate) {
                if let jsonString = infoObject.data {
                    if let jsonDict = convertToDictionary(text: jsonString),
                        let mapNormal = jsonDict["map_normal"] as? String {
                        vc.locationName = mapNormal
                        if let mapInvalid = jsonDict["map_invalid"] as? String {
                            vc.locationInvName = mapInvalid
                        }
                    }
                }
            }
        } else if segue.identifier == "showSupport" {
            let vc = segue.destination as! SupportViewController
            vc.station = station
            vc.venue = venue
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell",
                                                      for: indexPath) as! PhotoCell
        if let url = photosArray[indexPath.item].url {
            cell.photoImageView.kf.setImage(with: URL(string:url))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let browser = PhotoBrowser(photos: images)
        browser.initializePageIndex(indexPath.item)
        present(browser, animated: true, completion: {})
        
    }
    

    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionView {
            let x = collectionView.contentOffset.x
            let w = collectionView.bounds.size.width
            let currentPage = Int(ceil(x/w))
            
            photoCountLabel.text = "\(currentPage+1)"+"/"+"\(photosArray.count)"
        }
    }
    
    @IBAction func fixIssueButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "showSupport", sender: nil)
    }
}

extension NavigationServiceDetailViewController : TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        UIApplication.shared.openURL(url)
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWithPhoneNumber phoneNumber: String!) {
        let trimmedString = phoneNumber.replacingOccurrences(of: " ", with: "")
        let telUrl:NSURL? = NSURL(string:"telprompt:"+trimmedString)
        if ((telUrl) != nil){
            if(UIApplication.shared.canOpenURL(telUrl! as URL)){
                UIApplication.shared.openURL(NSURL(string: "telprompt:"+trimmedString)! as URL)
            }else
            {
                print("Call not available")
            }
        }
    }
}
