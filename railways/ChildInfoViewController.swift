//
//  ChildInfoViewController.swift
//  railways
//
//  Created by Artem Peskishev on 03.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class ChildInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var titleString : String?
    var station : RailwayStation?
    var parentObject : Info?
    var infoObjects = [Info]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let titleString = titleString {
            title = titleString
        }
        
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if let parentObject = parentObject {
            let predicate = NSPredicate(format: "id_parent = %@", parentObject.infoID!)
            if let childObjects = Info.mr_findAll(with: predicate) as? [Info] {
                infoObjects.append(parentObject)
                infoObjects = infoObjects + childObjects
            }
        }
    }
    
    // MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServicesCell
        cell.titleLabel.text = infoObjects[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedInfoObject = infoObjects[indexPath.row]
        
        if let type = selectedInfoObject.type {
            switch type {
            case "html":
                performSegue(withIdentifier: "showHTML", sender: selectedInfoObject)
                break
            case "link":
                performSegue(withIdentifier: "showLink", sender: selectedInfoObject)
                break
            case "json":
                if let name = selectedInfoObject.name {
                    switch name {
                    case "emergency_services":
                        performSegue(withIdentifier: "showEmergency", sender: selectedInfoObject)
                        break
                    case "photo":
                        performSegue(withIdentifier: "showPhotos", sender: selectedInfoObject)
                        break
                    case "services":
                        performSegue(withIdentifier: "showServices", sender: selectedInfoObject)
                        break
                    case "schedule":
                        performSegue(withIdentifier: "showSchedule", sender: selectedInfoObject)
                        break
                    case "availability_passport":
                        performSegue(withIdentifier: "showPassport", sender: selectedInfoObject)
                        break
                        
                    default:
                        break
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showHTML" {
            let infoObject = sender as! Info
            let vc = segue.destination as! WebViewViewController
            vc.station = station
            vc.htmlString = infoObject.data
            vc.titleString = infoObject.title
        } else if segue.identifier == "showLink" {
            let infoObject = sender as! Info
            let nvc = segue.destination as! UINavigationController
            let vc = nvc.visibleViewController as! LinkViewController
            vc.webURL = infoObject.data
        } else if segue.identifier == "showEmergency" {
            let infoObject = sender as! Info
            let vc = segue.destination as! EmergencyViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showPhotos" {
            let infoObject = sender as! Info
            let vc = segue.destination as! PhotosViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showServices" {
            let infoObject = sender as! Info
            let vc = segue.destination as! ServicesViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showSchedule" {
            let infoObject = sender as! Info
            let vc = segue.destination as! ScheduleViewController
            vc.station = station
            vc.titleString = infoObject.title
        } else if segue.identifier == "showPassport" {
            let infoObject = sender as! Info
            let vc = segue.destination as! PassportViewController
            vc.station = station
            vc.titleString = infoObject.title
        }
    }
    
}
