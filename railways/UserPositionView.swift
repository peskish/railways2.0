//
//  UserPositionView.swift
//  railways
//
//  Created by Artem Peskishev on 12.12.16.
//  Copyright © 2016 Enlighted. All rights reserved.
//

import UIKit

class UserPositionView: MapPoint {
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        self.originalFrame = frame
        self.backgroundColor = UIColor.clear
        self.setImage(UIImage(resourceName: "location_with_arrow"), for: .normal)
    }
    
    func rotateWith(azimuth: CGFloat) {
        
    }
    
    override func resizeWith(zoom: CGFloat) {
        
        self.center = CGPoint(x:originalCenter.x*zoom, y: originalCenter.y*zoom)

    }
}
