

Pod::Spec.new do |s|
    s.name         = "Stations"
    s.version      = "0.0.6"
    s.summary      = "A short description of Stations."
    s.description  = "StationsPod"
    s.homepage     = "http://EXAMPLE/Stations"
    s.license      =  "MIT"
    s.author       = { "RZD" => "" }
    s.platform     = :ios, "8.0"
    s.source       = { :git => "PATH_TO_PODSPEC", :tag => "master" }
    s.source_files  =  'railways/*.{swift}'
    s.exclude_files =  'railways/AppDelegate.swift'
    
    s.resource_bundles = {
        'Stations' => ['railways/**/*.storyboard', 'railways/*.xib', 'railways/**/*.otf', 'railways/*.xcassets', 'railways/**/*.ttf', 'railways/*.{xcdatamodeld, xcdatamodel}', 'railways/*.lproj/*.strings']
    }
     
    s.dependency 'Kingfisher'
    s.dependency 'MagicalRecord'
    s.dependency 'Alamofire'
    s.dependency 'MBProgressHUD'
    s.dependency 'MBAutoGrowingTextView'
    s.dependency 'ReachabilitySwift'
    s.dependency 'SKPhotoBrowser'
    s.dependency 'SevenSwitch'
    s.dependency 'MBProgressHUD'
    s.dependency 'DZNEmptyDataSet'
    s.dependency 'TTTAttributedLabel'
    s.dependency 'Navigine', '= 1.0.30'
    s.dependency 'Localize-Swift'
end
